# b-jim conda recipe

Home: "https://gitlab.esss.lu.se/nice/conda-bot-testbed/testbed-modules/b-jim"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS b-jim module
